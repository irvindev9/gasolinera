<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Nuevo Usuario</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>

<body>
    <h1>Nuevo registro</h1>
    <p>Un nuevo usuario acaba de realizar su registro con la siguiente información</p>
    <div class="card" style="width: 18rem;">
        <ul class="list-group list-group-flush">
            <li class="list-group-item"><strong>Nombre: </strong><span class="text-muted">{{ $msg->name}}</span></li>
            <li class="list-group-item"><strong>Telefono: </strong><span class="text-muted">{{ $msg->phone }}</span>
            </li>
            <li class="list-group-item"><strong>Correo electronico: </strong><span
                    class="text-muted">{{ $msg->email }}</span></li>
            <li class="list-group-item"><strong>Dirección: </strong><span class="text-muted">{{ $msg->address }}</span>
            </li>
            <li class="list-group-item"><strong>Ciudad: </strong><span class="text-muted">{{ $msg->city}}</span></li>
        </ul>
    </div>
</body>

</html>