<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Contacto</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>

<body>
    <h1>Alguien se ha puesto en contacto</h1>
    <p>Un usuario se ha puesto en contacto</p>
    <div class="card" style="width: 18rem;">
        <ul class="list-group list-group-flush">
            <li class="list-group-item"><strong>Nombre: </strong><span class="text-muted">{{ $msg->name}}</span></li>
            <li class="list-group-item"><strong>Empresa: </strong><span class="text-muted">{{ $msg->company }}</span>
            </li>
            <li class="list-group-item"><strong>Correo electronico: </strong><span
                    class="text-muted">{{ $msg->email }}</span></li>
            <li class="list-group-item"><strong>Mensaje: </strong><span class="text-muted">{{ $msg->message }}</span>
            </li>
        </ul>
    </div>
</body>

</html>