@extends('index')

@section('seccion')

<div class="container contenedor_gas_natural">
    <div class="row">
        <div class="col-10 offset-1">
            <h1>Gas Natural Vehícular</h1>
            <p>El Gas Natural Vehicular (GNV) es el único combustible abundante, amigo del medio ambiente, viable económicamente y con la infraestructura necesaria para abastecer a toda la flota mundial automotriz por los próximos 40 años.</p>
            <img src="/img/gas_natural_auto.jpg" width="100%" height="auto">
        </div>
    </div>
    <div class="row contenedor_lista_gas_natural">
        <div class="col-md-5 offset-md-1 col-12">
            <h3 class="color_lista_gas_natural">Económico</h3>
            <ul>
                <li>Ahorros de hasta un 45% respecto a otros combustibles.</li>
                <li>Alarga los mantenimientos de tu motor.</li>
                <li>Combustible libre de subsidios gubernamentales debido a su bajo costo.</li>
            </ul>
        </div>
        <div class="col-md-5 col-12">
            <h3 class="color_lista_gas_natural">Seguro</h3>
            <ul>
                <li>Al ser más liviano que el aire, ante una eventual pérdida, el gas natural comprimido se eleva y se disipa rápidamente.</li>
                <li>Menos inflamable que otros combustibles.</li>
                <li>Cilindros de almacenaje con los más altos estándares de calidad y confiabilidad.</li>
            </ul>
        </div>
    </div>
    <div class="row">
        <div class="col-md-5 offset-md-1 col-12">
            <h3 class="color_lista_gas_natural">Ecológico</h3>
            <ul>
                <li>El combustible de uso masivo menos contaminante.</li>
                <li>30% menos de CO2 causante del "efecto invernadero".</li>
                <li>95%menos monóxido de carbono.</li>    
                <li>30% menos óxidos de nitrógeno.</li>
                <li>No contiene azufre (SOx), plomo orgánico, ni benceno (C6H6).</li>
                <li>Reducción de emisiones sonoras.</li>
                <li>No es tóxico, ni corrosivo.</li>
            </ul>
        </div>    
        <div class="col-md-5 col-12">
            <h3 class="color_lista_gas_natural">Infraestructura</h3>
            <ul>
                <li>Más de 18 millones de vehículos circulan con Gas Natural Vehicular en el mundo.</li>
                <li>70 años en el mercado.</li>
                <li>80 países en todo el mundo utilizan Gas Natural Vehicular.</li>
                <li>Constante expansión de gasoductos para abastecer la creciente demanda.</li>
                <li>México cuenta con la cuarta reserva más grande de Gas Natural en el mundo.</li>
            </ul>
        </div>
    </div>
</div>
    
@endsection