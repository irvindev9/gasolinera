@extends('index')

@section('seccion')

<div class="container">
    <div class="row" style="margin-top:3em;">
        <div class="col-12" style="margin-top:1em;">
            <div class="row">
                <div class="col-12 text-left" style="font-size:1.4em;">
                    Facturación en oficinas de lunes a sábado de 9:00 am a 2:00 pm.
                </div>

                <div class="col-12" style="color:grey;">
                        <br><b class="flux"">Dirección:</b><br>
                        Avenida López Portillo<br>
                        {{-- Ejido La Quemada <br> --}}
                        Cancún, Quintana Roo <br>
                        Tel: (555) 102-6036<br>
                        Para más información, escríbenos a vivian.echemendia@engie.com<br><br>

                        *Facturas mensuales hasta el último día hábil del mes.<br>
                </div>
            </div>
        </div>
    </div>
</div>

<br><br><br><br><br><br><br><br><br>

@endsection