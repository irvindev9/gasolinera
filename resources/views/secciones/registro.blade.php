@extends('index')

@section('seccion')

<div class="container">
    <div class="row" style="margin-top:3em;">
        <div class="col-12 col-lg-5">
            <div class="row">
                <div class="col-12 text-center" style="font-size:1.5em;">
                        Beneficios de usar<br><b> GNV</b>
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-sm-6 col-md-6 col-lg-6" style="margin-top:1em;">
                    <div class="row">
                        <div class="col-12 text-center">
                            <img width="136px" height="auto" src="{{URL::asset('../img/hoja_engie.png')}}">
                        </div>
                    </div>
                    <div class="row" style="margin-top:0.5em; font-size:0.9em;">
                        <div class="col-12 text-center">
                            Engie GNV® te ofrece cómodos esquemas de financiamiento para tu conversión a GNV.
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-6 col-lg-6" style="margin-top:1em;">
                    <div class="row">
                        <div class="col-12 text-center">
                            <img src="{{URL::asset('../img/registro/ahorro.png')}}">
                        </div>
                    </div>
                    <div class="row" style="margin-top:0.5em; font-size:0.9em;">
                        <div class="col-12 text-center">
                            +45% En Ahorros Vs combustibles convencionales (Gasolina / Diesel).
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-12 col-sm-6 col-md-6 col-lg-6" style="margin-top:1em;">
                    <div class="row">
                        <div class="col-12 text-center">
                            <img src="{{URL::asset('../img/registro/costos.png')}}">
                        </div>
                    </div>
                    <div class="row" style="margin-top:0.5em; font-size:0.9em;">
                        <div class="col-12 text-center">
                            Reduce los costos de mantenimiento y no daña tu motor.
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-6 col-lg-6" style="margin-top:1em;">
                    <div class="row">
                        <div class="col-12 text-center">
                            <img src="{{URL::asset('../img/registro/emision.png')}}">
                        </div>
                    </div>
                    <div class="row" style="margin-top:0.5em; font-size:0.9em;">
                        <div class="col-12 text-center">
                            Disminuye la emisión de gases contaminantes.
                        </div>
                    </div>
                </div>
            </div>

            <div class="row" style="margin-bottom:3em;">
                <div class="col-12 col-sm-6 col-md-6 col-lg-6" style="margin-top:1em;">
                    <div class="row">
                        <div class="col-12 text-center">
                            <img src="{{URL::asset('../img/registro/manipulable.png')}}">
                        </div>
                    </div>
                    <div class="row" style="margin-top:0.5em; font-size:0.9em;">
                        <div class="col-12 text-center">
                            Combustible no manipulable en estaciones.
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-6 col-lg-6" style="margin-top:1em;">
                    <div class="row">
                        <div class="col-12 text-center">
                            <img src="{{URL::asset('../img/registro/normas.png')}}">
                        </div>
                    </div>
                    <div class="row" style="margin-top:0.5em; font-size:0.9em;">
                        <div class="col-12 text-center">
                            Cumple con exigentes normas de seguridad.
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="col-12 col-lg-7">
            <div class="row">
                <div class="col-12 text-center" style="font-size:1.5em;">
                    <b class="flux" style="font-size:1.6em;">¡Súmate al cambio con nosotros!</b>
                    <br>
                    Llena tus datos para recibir más información
                </div>
                @isset($_GET['ok'])
                    <div class="col-12 text-center alert alert-success">
                        ¡Enviado, gracias! 
                    </div>
                @endif
            </div>

            <div class="row" style="margin-top:2em; margin-bottom:3em;">
                <div class="col-12 col-lg-10 offset-lg-1">
                    <form action="/registro/submit" method="GET">
                        <div class="form-group">
                            <label for="nombre">Nombre:</label>
                            <input type="text" class="form-control" name="name" id="nombre">
                        </div>
                        <div class="form-group">
                            <label for="telefono">Teléfono:</label>
                            <input type="text" class="form-control" name="phone" id="telefono">
                        </div>
                        <div class="form-group">
                            <label for="correo_electronico">Correo electrónico:</label>
                            <input type="email" class="form-control" name="email" id="correo_electronico">
                        </div>
                        <div class="form-group">
                            <label for="direccion">Dirección:</label>
                            <input type="text" class="form-control" name="address" id="direccion">
                        </div>
                        <div class="form-group">
                            <label for="ciudad">Ciudad:</label>
                            <input type="text" class="form-control" name="city" id="ciudad">
                        </div>

                        <div class="row">
                            <div class="container d-flex justify-content-center">
                                <div class="col-12 col-sm-8 col-md-6 col-lg-6">
                                    <button type="submit" class="btn btn-block btn-primary btn-flux">Enviar</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection