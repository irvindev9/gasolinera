@extends('index')

@section('seccion')

<div class="container">
    <div class="row" style="margin-top:3em;">
        <div class="col-12 col-md-6 col-lg-5" style="margin-top:1em;">
            <div class="row">
                <div class="col-12 text-left" style="font-size:1.7em;">
                        Estación Matriz
                </div>
            </div>
            <div class="row">
                <div class="col-12" style="margin-top:1em;">
                    <img style="width:100%; height:auto;" src="{{URL::asset('../img/registro/ubicacion.jpg')}}">
                </div>
                <div class="col-12" style="margin-top:1em;">
                    <div class="container">
                        <div class="row" style="color:grey; font-size:0.9em;">
                            <div class="col-12 col-lg-6">
                                <b class="flux"">Dirección Estación Matriz</b><br>
                                Avenida López Portillo<br>
                                {{-- Ejido La Quemada <br> --}}
                                Cancún, Quintana Roo<br>
                            </div>

                            <div class="col-12 col-lg-6">
                                <b class="flux"">Contacto</b><br>
                                Tel: (555) 102-6036 | vivian.echemendia@engie.com<br>
                                {{-- Cel: 045 (443) 231-5345<br> --}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="col-12 col-md-6 col-lg-5 offset-lg-1" style="margin-top:1em;">
            <div class="row">
                <div class="col-12 text-center" style="font-size:1.5em;">
                    <b class="flux" style="font-size:1.6em;">Contáctanos</b>
                </div>
                @isset($_GET['ok'])
                    <div class="col-12 text-center alert alert-success">
                        ¡Enviado, gracias! 
                    </div>
                @endif
            </div>

            <div class="row" style="margin-top:2em; margin-bottom:3em;">
                <div class="col-12 col-lg-10 offset-lg-1">
                    <form action="/contacto/submit" method="GET">
                        <div class="form-group">
                            <label for="nombre">¿Cuál es tu nombre?</label>
                            <input type="text" class="form-control" name="name" id="nombre">
                        </div>
                        <div class="form-group">
                            <label for="compañia">Compañía</label>
                            <input type="text" class="form-control" name="company" id="compañia">
                        </div>
                        <div class="form-group">
                            <label for="correo_electronico">Correo electrónico</label>
                            <input type="email" class="form-control" name="email" id="correo_electronico">
                        </div>
                        <div class="form-group">
                            <label for="mensaje">Escriba un mensaje</label>
                            <input type="text" class="form-control" name="message" id="mensaje">
                        </div>


                        <div class="row">
                            <div class="container d-flex justify-content-center">
                                <div class="col-12 col-sm-8 col-md-6 col-lg-6">
                                    <button type="submit" class="btn btn-block btn-primary btn-flux">Enviar</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection