@extends('index')

@section('seccion')
<style>
    .Div_flotante {
        background-color: transparent;
        float: right!important;
        position: fixed!important;
        bottom: 0px!important;
        right: 0%!important;
        width: 20%;
        height: auto;
        z-index: 5!important;
    }

    .position_card{
        float: right!important;
        width: 100%!important;
    }

    .errors_message{
        font-size: 10px;
    }
    
    .Div_flotante{
        display: none;
    }

    .close_btn{
    background-color: transparent!important;
    border-color: transparent!important;
    position: absolute!important;
    width: 3%;
    height: auto;
    float: right!important;
    }
    .btn_message{
        background-color: #00AEEF;
        borde
        r-color: transparent;
    }
    @media only screen and (max-width: 600px) {
    .Div_flotante {
        display: none!important;
    }
}
</style>
<div class="row fix-row">
    <div class="col-12 fix-col">
        <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img class="d-block w-100" src="/img/Slide01v2.jpg" alt="First slide">
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100" src="/img/Slide02v2.jpg" alt="Second slide">
                </div>
                {{-- <div class="carousel-item">
                    <img class="d-block w-100" src="/img/slide4.jpg" alt="Third slide">
                    </div> --}}
            </div>
            <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
</div>

<div class="Div_flotante">
    <div class="card position_card" style="width: 18rem;">
        <a class="btn btn-primary close_btn">
            &#10006;
        </a>
        <div class="errors_message">@if ( $errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
        </div>

        <div class="card-body">
            <br>
            <h6 class="card-title">Asistencia en linea</h6>
            <p class="card-text">Como podemos ayudarte?</p>
            <form action="{{route('help_message')}}" method="POST">
                <div class="form-group">
                    @csrf
                    <div class="form-group">
                        <input type="text" class="form-control" required placeholder="Tu nombre" name="name">
                        <input type="email" class="form-control" required placeholder="Correo electronico" name="email">
                        <label for="exampleFormControlTextarea1"></label>
                        <textarea class="form-control" required name="message" placeholder="" id="exampleFormControlTextarea1"
                            rows="2"></textarea>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary btn_message">Enviar</button>
            </form>
        </div>
    </div>
</div>

<div class="row fix-row row-tools">
    <div class="col-md-6 col-lg-3 col-12 tool-blue tool-mb-1" data-toggle="modal" data-target="#exampleModalLong">
        <div class="row">
            <div class="col-2">
                <a href="#"><img class="img-v" src="/img/icono_calculadora.png" width="37px" height="auto"></a>
            </div>
            <div class="col-10 white v-a">
                Calculadora del ahorro
                <small class="disabled">
                    <br>
                    Averigua cuánto puedes ahorrar >
                </small>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-lg-3 col-12 tool-blue tool-mb-2" onclick="window.location.href='/conversiones'">
        <div class="row">
            <div class="col-2">
                <a href="#"><img class="img-v" src="/img/iconoConversion.png" width="60px" height="auto"></a>
            </div>
            <div class="col-10">
                <div class="col-10 white v-a">
                    Conversión Vehicular
                    <small class="disabled">
                        <br>
                        Conoce mas sobre los beneficios >
                    </small>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-lg-3 col-12 tool-blue tool-mb-3" onclick="window.location.href='/estaciones'">
        <div class="row">
            <div class="col-2">
                <a href="#"><img class="img-v" src="/img/iconoEstaciones.png" width="35px" height="auto"></a>
            </div>
            <div class="col-10">
                <div class="col-10 white v-a">
                    Estaciones de servicio
                    <small class="disabled">
                        <br>
                        Ubica tu estación mas cercana >
                    </small>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-lg-3 col-12 tool-blue tool-mb-4" onclick="window.location.href='/facturacion'">
        <div class="row">
            <div class="col-2">
                <a href="#"><img class="img-v" src="/img/iconoBeneficios.png" width="75px" height="auto"></a>
            </div>
            <div class="col-10">
                <div class="col-10 white v-a">
                    Facturación
                    <small class="disabled">
                        <br>
                        Pide tu facturación aqui >
                    </small>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row fix-row circulos_contenedor">
    <div class="col-12 fix-col">
        <div class="container">
            <div class="row">
                <div class="col-lg-2 col-md-4 col-6">
                    <center>
                        <img class="circulo_six" width="85%" height="auto" src="/img/hoja_engie.png">
                        <br>
                        <p class="body-txt">
                            Engie GNV® te ofrece cómodos esquemas de financiamiento para tu conversión a GNV
                        </p>
                    </center>
                </div>
                <div class="col-lg-2 col-md-4 col-6">
                    <center>
                        <img class="circulo_six" width="85%" height="auto" src="/img/circulo2.png">
                        <br>
                        <p class="body-txt">
                            + 45% En Ahorros Vs combustibles convencionales
                            (Gasolina / Diesel)
                        </p>
                    </center>
                </div>
                <div class="col-lg-2 col-md-4 col-6">
                    <center>
                        <img class="circulo_six" width="85%" height="auto" src="/img/circulo3.png">
                        <br>
                        <p class="body-txt">
                            Reduce los costos de mantenimiento y no daña tu motor.
                        </p>
                    </center>
                </div>
                <div class="col-lg-2 col-md-4 col-6">
                    <center>
                        <img class="circulo_six" width="85%" height="auto" src="/img/circulo4.png">
                        <br>
                        <p class="body-txt">
                            Disminuye la emisión de gases contaminantes.
                        </p>
                    </center>
                </div>
                <div class="col-lg-2 col-md-4 col-6">
                    <center>
                        <img class="circulo_six" width="85%" height="auto" src="/img/circulo5.png">
                        <br>
                        <p class="body-txt">
                            Combustible no manipulable en estaciones.
                        </p>
                    </center>
                </div>
                <div class="col-lg-2 col-md-4 col-6">
                    <center>
                        <img class="circulo_six" width="85%" height="auto" src="/img/circulo6.png">
                        <br>
                        <p class="body-txt">
                            Cumple con exigentes normas de seguridad.
                        </p>
                    </center>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle"
    aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <center class="calculadora_loader d-none" style="margin-top:2em;">
                <div class="spinner-border text-primary" role="status">
                    <span class="sr-only">Loading...</span>
                </div>
            </center>
            <div class="modal-header header_modal" style="background-color:#00AEEF;">
                <div class="container">
                    <div class="row">
                        <div class="col-12 text-center" style="color:white; font-size:1.4em;">
                            CALCULADORA DE AHORRO
                        </div>
                    </div>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="container before_calcular">
                    <div class="row">
                        <div class="col-12 text-center efect" style="color:grey; font-size:0.9em;">
                            <span style="color:#29746f; font-size:1.4em;">Averigua cuánto puedes ahorrar con Engie
                                GNV</span> <br>
                            Ingresa tu consumo semanal actual en litros o pesos
                        </div>
                    </div>

                    <div class="row d-flex justify-content-center" style="margin-top:1em;">
                        <div class="col-12 col-md-12 col-lg-5"
                            style="margin-top:1em; border-right:solid 0.1px #E9ECEF;">
                            <label for="" class="col-12 col-lg-9 offset-lg-1"
                                style="font-size:0.9em; color:grey;">Consumo Semanal en Litros:</label>
                            <div class="input-group col-12 col-lg-9 offset-lg-1">
                                <input type="number" class="form-control" id="consumo_litros"
                                    style="height:3em; border-radius:5px 0px 0px 5px;">
                                <div class="input-group-append">
                                    <span class="input-group-text" id="basic-addon2"
                                        style="padding-right:0.3em!important; padding-left:0.3em!important; padding-top:0.9em; font-size:0.9em; font-weight:normal; border-radius:0px 5px 5px 0px; color:grey;">L</span>
                                </div>
                                <div class="invalid-feedback">
                                    Solo se permiten valores enteros (No decimales)
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-12 col-lg-5" style="margin-top:1em;">
                            <label for="" class="col-12 col-lg-9 offset-lg-2"
                                style="font-size:0.9em; color:grey;">Consumo Semanal en Pesos:</label>
                            <div class="input-group col-12 col-lg-9 offset-lg-2">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"
                                        style="padding-right:0.3em!important; padding-left:0.3em!important; padding-top:0.9em; font-size:0.9em; font-weight:normal; border-radius:5px 0px 0px 5px; color:grey;">$</span>
                                </div>
                                <input type="number" class="form-control" id="consumo_pesos" style="height:3em;">
                                <div class="input-group-append">
                                    <span class="input-group-text"
                                        style="padding-right:0.3em!important; padding-left:0.3em!important; padding-top:0.9em; font-size:0.9em; font-weight:normal; border-radius:0px 5px 5px 0px; color:grey;">.00</span>
                                </div>
                                <div class="invalid-feedback">
                                    Solo se permiten valores enteros (No decimales)
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row d-flex justify-content-center" style="margin-top:1.5em;">
                        <div class="col-12 col-md-8 col-lg-6">
                            <button type="button" id="btn_calcular" class="btn btn_calculadora btn-block">Calcular
                                ahorro</button>
                        </div>
                    </div>
                </div>

                <div class="container after_calcular d-none">
                    <div class="row">
                        <div class="col-12 text-center" style="color:grey; font-size:0.9em;">
                            <span style="color:#29746f; font-size:1.4em;">Con Engie GNV podrías ahorrar:
                        </div>
                    </div>

                    <div class="row" style="margin-top:1em;">
                        <div class="col-12 text-center" style="margin-top:1em;">
                            <b class="label_resultado" style="color:#54A939; font-size:1.8em">$2,184.00</b>
                            <br>
                            <b style="color:#29746f; font-size:1.4em">Mensuales</b>
                        </div>
                    </div>

                    <div class="row d-flex justify-content-center" style="margin-top:1.5em;">
                        <div class="col-12 text-center">
                            <a href="#!" id="nuevo_calculo" style="font-size:0.9em;"><span><img
                                        style="height:1.5em; width:auto;"
                                        src="{{URL::asset('../img/refresh_icon.png')}}"> Volver a calcular</span></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer footer_modal" style="background-color:#E9ECEF; margin-top:2em;">
                <div class="container">
                    <div class="row">
                        <div class="col-12 col-md-6 text-center" style="font-size:1.2em;">
                            Gasolina Magna: <b style="color:black; font-size:1.25em;">$10.29</b>
                        </div>
                        <div class="col-12 col-md-6 text-center" style="font-size:1.2em;">
                            Gas Natural GNV: <b style="color:#275872; font-size:1.25em;">$10.29</b>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $('#btn_calcular').on('click', function () {

        var total = 0;

        //Se verifica que cantidad ingresada no sea decimal
        var patron = /^\d*$/;

        //Se valida que campos tengan cantidad y no esten vacios
        if ($('#consumo_litros').val().trim() != '' || $('#consumo_pesos').val().trim() != '') {

            //Se valida que sean cantidades enteras y no decimales
            if (patron.test($('#consumo_litros').val().trim()) && patron.test($('#consumo_pesos').val()
                    .trim())) {
                //Se oculta contenido de modal
                $('.before_calcular').addClass('d-none');
                $('.header_modal').addClass('d-none');
                $('.footer_modal').addClass('d-none');

                //Se muestra loader
                $('.calculadora_loader').removeClass('d-none');

                //Se comienza calculo de ahorro
                if ($('#consumo_litros').val().trim() != '') {

                    total = ($('#consumo_litros').val() * 4 * 10.29) - ($('#consumo_litros').val() * 4 * 10.29);
                    $('.label_resultado').html(numeral(total).format('$0,0.00'));

                } else {
                    total = ($('#consumo_pesos').val() * 4) - ((($('#consumo_pesos').val() / 10.29) * 10.29) *
                        4);
                    $('.label_resultado').html(numeral(total).format('$0,0.00'));
                }

                //espera 1 segundo para mostrar resultado
                setTimeout(
                    function () {
                        //oculto loader
                        $('.calculadora_loader').addClass('d-none');
                        //muestro resultados
                        $('.after_calcular').removeClass('d-none');
                        $('.header_modal').removeClass('d-none');
                        $('.footer_modal').removeClass('d-none');
                        $('#consumo_litros').val('');
                        $('#consumo_pesos').val('');
                    }, 400);
            } else {

                if ($('#consumo_litros').val().trim() != '') {
                    $('#consumo_litros').addClass('is-invalid');
                    $('#feed_consumo_litros').html('Solo se permiten valores enteros (No decimales)');
                } else {
                    $('#consumo_pesos').addClass('is-invalid');
                    $('#feed_consumo_pesos').html('Solo se permiten valores enteros (No decimales)');
                }

            }

        } else {
            $('#consumo_litros').addClass('is-invalid');
            $('#consumo_pesos').addClass('is-invalid');
            $('#feed_consumo_litros').html('Ingrese cantidad valida');
            $('#feed_consumo_pesos').html('Ingrese cantidad valida');
        }
    });

    $('#nuevo_calculo').on('click', function () {
        //Se oculta contenido de modal
        $('.after_calcular').addClass('d-none');
        $('.header_modal').addClass('d-none');
        $('.footer_modal').addClass('d-none');

        //Se dejan valores en blanco
        $('#consumo_litros').val('');
        $('#consumo_pesos').val('');

        //Se muestra loader
        $('.calculadora_loader').removeClass('d-none');

        //espera 1 segundo para mostrar resultado
        setTimeout(
            function () {
                //oculto loader
                $('.calculadora_loader').addClass('d-none');
                //muestro resultados
                $('.before_calcular').removeClass('d-none');
                $('.header_modal').removeClass('d-none');
                $('.footer_modal').removeClass('d-none');
                $('#consumo_litros').val('');
                $('#consumo_pesos').val('');
            }, 400);
    });

    $('#consumo_litros').on('keydown', function () {
        $('#consumo_pesos').val('');
        $(this).removeClass('is-invalid');
        $('#consumo_pesos').removeClass('is-invalid');
    });

    $('#consumo_pesos').on('keydown', function () {
        $('#consumo_litros').val('');
        $(this).removeClass('is-invalid');
        $('#consumo_litros').removeClass('is-invalid');
    });

    $(document).ready(function () {
        setTimeout(function () {
            $('.Div_flotante').show().fadeIn(); // or fade, css display however you'd like.
        }, 5000);

        $(".close_btn").click(function () {
            $(".Div_flotante").hide();
        });
    });

    
</script>


@endsection