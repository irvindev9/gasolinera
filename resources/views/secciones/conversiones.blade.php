@extends('index')

@section('seccion')

<div class="container">
    <div class="row">
        <div class="col-lg-10 offset-lg-1 col-12 contenedor_verde_conversiones">
            0% enganche
            <br>
            ¡La conversión se paga sola!
        </div>
    </div>
    <div class="row">
        <div class="col-lg-5 offset-lg-1 col-12 contenedor_conversiones_texto">
            <h1>Conversión SEDAN</h1>
            Equipos de conversión dual gasolina / gas natural vehicular
            <br>
            Características: 4 cilindros.
            <br>
            Ahorro de hasta un 45% que otros combustibles convencionales
            <br>
            <img src="/img/Taxi_Engie.png" width="100%" height="auto">
        </div>
        <div class="col-lg-5 col-12 contenedor_conversiones_texto">
            <h1>Conversión tipo VAN</h1>
            Equipos de conversión dual gasolina / gas natural vehicular
            <br>
            Características: 4 cilindros.
            <br>
            Ahorro de hasta un 45% que otros combustibles convencionales
            <br>
            <img src="/img/Combie_Engie_Urvan.png" width="100%" height="auto">
        </div> .
    </div>
    <div class="row">
        <div class="col-lg-10 offset-lg-1 col-12">
            <h1>Esquemas de financiamiento</h1>
        </div>
        <div class="col-lg-10 offset-lg-1 col-12 contenedor_3_conversiones">
            <div class="row">
                <div class="col-lg-4 col-12">
                    <h3 class="color_lista_conversiones">1- Pago del equipo de Conversión de GNV de contado.</h3>
                    <ul>
                        <li>Beneficios directos en el ahorro de combustible desde tu primera carga.</li>
                    </ul>
                </div>
                <div class="col-lg-4 col-12">
                    <h3 class="color_lista_conversiones">2- Conversión a GNV con Financiamiento.</h3>
                    <ul>
                        <li>0 % Enganche.</li>
                        <li>Abono a tu financiamiento en cada carga de GNV.</li>
                    </ul>
                </div>
                <div class="col-lg-4 col-12">
                    <h3 class="color_lista_conversiones">3- Financiamiento de Vehículo Nuevo convertido a GNV.</h3>
                    <ul>
                        <li>Enganches variables.</li>
                        <li>Abono a tu financiamiento en cada carga de GNV.</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="row contenedor_dudas">
        <div class="col-lg-10 offset-lg-1 col-12">
            <h1>Dudas frecuentes</h1>
            <div class="accordion" id="accordionExample">
                <div class="card lista_verde">
                    <div class="card-header no-padding" id="headingOne">
                        <h5 class="mb-0">
                            <button class="btn btn-link quitar-a" type="button" data-toggle="collapse" data-target="#collapseOne"
                                aria-expanded="true" aria-controls="collapseOne">
                                ¿Ahorro dinero usando GNV?
                            </button>
                        </h5>
                    </div>

                    <div id="collapseOne" class="collapse" aria-labelledby="headingOne"
                        data-parent="#accordionExample">
                        <div class="card-body">
                            Sí, usando GNV se ahorra hasta un 45% respecto al precio de los combustibles convencionales.
                        </div>
                    </div>
                </div>
                <div class="card lista_verde">
                    <div class="card-header no-padding" id="headingTwo">
                        <h5 class="mb-0">
                            <button class="btn btn-link quitar-a collapsed" type="button" data-toggle="collapse"
                                data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                ¿Si me convierto a GNV, ¿tengo que usarlo siempre?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                        <div class="card-body">
                                No precisamente, los vehículos convertidos son duales. Significa que el vehículo funciona con GNV y Gasolina.
                        </div>
                    </div>
                </div>
                <div class="card lista_verde">
                    <div class="card-header no-padding" id="headingThree">
                        <h5 class="mb-0">
                            <button class="btn btn-link quitar-a collapsed" type="button" data-toggle="collapse"
                                data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                ¿El GNV es sólo para vehículos de alta potencia?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree"
                        data-parent="#accordionExample">
                        <div class="card-body">
                            No, el GNV es un combustible adaptable a todos los vehículos de motor de combustión interna, desde autobuses y camionetas hasta automóviles. Depende del alto consumo de gasolina para que el propietario decida el cambio a GNV.
                        </div>
                    </div>
                </div>
                <div class="card lista_verde">
                    <div class="card-header no-padding" id="headingFour">
                        <h5 class="mb-0">
                            <button class="btn btn-link quitar-a collapsed" type="button" data-toggle="collapse"
                                data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                ¿Usando GNV el motor pierde potencia?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseFour" class="collapse" aria-labelledby="headingFour"
                        data-parent="#accordionExample">
                        <div class="card-body">
                            Sí, la pérdida de potencia puede variar del 5% al 10%. Depende principalmente de la altura de operación, el estado del motor y lo más importante, de la tecnología aplicada con GNV. Para convertir el vehículo se determina el estado del motor mediante una inspección (pre conversión).
                        </div>
                    </div>
                </div>
                <div class="card lista_verde">
                    <div class="card-header no-padding" id="headingFive">
                        <h5 class="mb-0">
                            <button class="btn btn-link quitar-a collapsed" type="button" data-toggle="collapse"
                                data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                ¿El GNV daña el motor?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseFive" class="collapse" aria-labelledby="headingFive"
                        data-parent="#accordionExample">
                        <div class="card-body">
                            No, antes de convertir el vehículo debe ser inspeccionado el motor mediante una serie de pruebas que califican su capacidad de adaptación al GNV (pre conversión). El GNV no es corrosivo, la combustión es más limpia y eficiente. La vida útil del motor es exactamente la misma.
                        </div>
                    </div>
                </div>
                <div class="card lista_verde">
                    <div class="card-header no-padding" id="headingSix">
                        <h5 class="mb-0">
                            <button class="btn btn-link quitar-a collapsed" type="button" data-toggle="collapse"
                                data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                                ¿Existen riesgos de seguridad con el GNV?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseSix" class="collapse" aria-labelledby="headingSix"
                        data-parent="#accordionExample">
                        <div class="card-body">
                            No, el GNV se disipa en la atmósfera rápidamente por ser más liviano que el aire a diferencia del Gas L.P. por ejemplo. El GNV tiene una temperatura de ignición de 600°C, mientras que la gasolina por su parte es de 450°C, por lo tanto, el GNV es mucho menos inflamable y más seguro que los combustibles convencionales.
                        </div>
                    </div>
                </div>
                <div class="card lista_verde">
                    <div class="card-header no-padding" id="headingSeven">
                        <h5 class="mb-0">
                            <button class="btn btn-link quitar-a collapsed" type="button" data-toggle="collapse"
                                data-target="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                                ¿Los cilindros son seguros?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseSeven" class="collapse" aria-labelledby="headingSeven"
                        data-parent="#accordionExample">
                        <div class="card-body">
                            Sí, soportan fuertes impactos y altas temperaturas, siendo más seguros y resistentes que los tanques de gasolina y cilindro de Gas LP. Están sujetos a ensayos de acuerdo a normativas internacionales imitando condiciones de abuso severo, tales como exposición extrema al calor, fuego, presión, disparos de armas de fuego y choques.
                        </div>
                    </div>
                </div>
                <div class="card lista_verde">
                    <div class="card-header no-padding" id="headingEight">
                        <h5 class="mb-0">
                            <button class="btn btn-link quitar-a collapsed" type="button" data-toggle="collapse"
                                data-target="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
                                ¿Es más costoso el mantenimiento?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseEight" class="collapse" aria-labelledby="headingEight"
                        data-parent="#accordionExample">
                        <div class="card-body">
                            No, se mantiene el mantenimiento preventivo normal del vehículo. Existe una revisión anual de componentes de sistema GNCV (Kit y tanques) y una verificación que no son costosas.
                        </div>
                    </div>
                </div>
                <div class="card lista_verde">
                    <div class="card-header no-padding" id="headingNine">
                        <h5 class="mb-0">
                            <button class="btn btn-link quitar-a collapsed" type="button" data-toggle="collapse"
                                data-target="#collapseNine" aria-expanded="false" aria-controls="collapseNine">
                                ¿Qué diferencia hay entre el Gas Natural y el Gas LP?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseNine" class="collapse" aria-labelledby="headingNine"
                        data-parent="#accordionExample">
                        <div class="card-body">
                            Muchas, pero fundamentalmente es que son distintos combustibles, el Gas Natural como su nombre lo dice es de origen fósil y se le denomina Gas Natural por que en su constitución química no intervine ningún proceso añadido como en los combustibles convencionales (refinación en gasolinas y diésel, licuefacción en Gas LP). Manteniendo así estabilidad en los precios del Gas Natural.
                        </div>
                    </div>
                </div>
                <div class="card lista_verde">
                    <div class="card-header no-padding" id="headingTeen">
                        <h5 class="mb-0">
                            <button class="btn btn-link quitar-a collapsed" type="button" data-toggle="collapse"
                                data-target="#collapseTeen" aria-expanded="false" aria-controls="collapseTeen">
                                ¿Qué otro uso tiene el Gas Natural?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTeen" class="collapse" aria-labelledby="headingTeen"
                        data-parent="#accordionExample">
                        <div class="card-body">
                            Se utiliza exitosamente como combustible en mundo en el área industrial, energética, doméstica y vehicular.
                        </div>
                    </div>
                </div>
                <div class="card lista_verde">
                    <div class="card-header no-padding" id="headingEleven">
                        <h5 class="mb-0">
                            <button class="btn btn-link quitar-a collapsed" type="button" data-toggle="collapse"
                                data-target="#collapseEleven" aria-expanded="false" aria-controls="collapseEleven">
                                ¿Qué me asegura que se mantengan los precios bajos en el Gas Natural?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseEleven" class="collapse" aria-labelledby="headingEleven"
                        data-parent="#accordionExample">
                        <div class="card-body">
                            El hecho de que su precio no está ligado a los precios internacionales del petróleo como es el caso de la gasolina, diésel y gas LP. Además de lo previamente mencionado, no tiene un proceso agregado que incremente su costo.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection