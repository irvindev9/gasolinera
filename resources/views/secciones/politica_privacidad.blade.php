@extends('index')

@section('seccion')

<div class="container">
    <div class="row" style="margin-top:3em;">
        <div class="col-12" style="font-size:1.7em;">
            Política de Privacidad
        </div>

        <div class="col-12" style="margin-top:2em; color:grey;">
            <b style="color:black">SECCIÓN 1 - ¿QUÉ HACEMOS CON TU INFORMACIÓN?</b><br><br>
            Cuando compras algo de nuestra tienda, como parte del proceso de compra-venta, nosotros recolectamos la información personal que nos das tales como nombre, dirección y correo electrónico.<br><br>
            
            Cuando navegas en nuestra tienda, también recibimos de manera automática la dirección de protocolo de internet de tu computadora (IP) con el fin de proporcionarnos información que nos ayuda a conocer acerca de su navegador y sistema operativo.<br><br>
            
            Con tu permiso, podremos enviarte correos electrónicos acerca de nuestra tienda, nuevos productos y otras actualizaciones.
        </div>

        <div class="col-12" style="margin-top:2em; color:grey;">
            <b style="color:black;">SECCIÓN 2 - CONSENTIMIENTO</b><br><br>
            <b style="font-size:0.9em; color:black;">¿Cómo obtienen mi consentimiento?</b><br><br>
            Cuando nos provees tu información personal para completar una transacción, verificar tu tarjeta de crédito, crear una órden, concertar un envío o hacer una devolución, implicamos que aceptas la recolección y uso por esa razón específica solamente.<br><br>
            
            Si te pedimos tu información personal por una razón secundaria, como marketing, te pediremos directamente tu expreso consentimiento, o te daremos la oportunidad de negarte.<br><br>
            
            <b style="font-size:0.9em; color:black">¿Cómo puedo anular mi consentimiento?</b><br><br>

            Si luego de haber aceptado cambias de opinión, puedes anular tu consentimiento para que te contactemos, por la recolección, uso o divulgación de tu información, en cualquier momento, contactándonos a vivian@engiegnv.com o escribiéndonos a: Engie GNV Avenida López Portillo, Cancún, Quintana Roo, Mexico.
        </div>

        <div class="col-12" style="margin-top:2em; color:grey;">
            <b style="color:black">SECCIÓN 3 - DIVULGACIÓN</b><br><br>
            Podemos divulgar tu información personal si se nos requiere por ley o si violas nuestros Términos de Servicio.<br>
        </div>

        <div class="col-12" style="margin-top:2em; color:grey;">
            <b style="color:black">SECCIÓN 4 - SHOPIFY</b><br><br>
            Nuestra tienda se encuentra alojada en Shopify Inc. Ellos nos proporcionan la plataforma de comercio electrónico en línea que nos permite venderte nuestros productos y servicios.<br><br>

            Tus datos se almacenan a través del almacenamiento de datos de Shopify, bases de datos y la aplicación general de Shopify. Tus datos se almacenan en un servidor seguro detrás de un firewall.<br><br>

            <b style="color:black; font-size:0.9em;">Pago:</b><br><br>

            Si eliges una pasarela de pago directo para completar tu compra, entonces Shopify almacena datos de tu tarjeta de crédito. Está cifrada a través de la Seguridad Standard de Datos de la Industrai de Tarjetas de Pago (PCI-DSS). Tus datos de transacción de compra se almacenan sólo en la medida en que sea necesario para completar la transacción de compra. Después de que se haya completado, se borra la información de su transacción de compra.<br><br>
            Todas las pasarelas de pago directo se adhieren a los estándares establecidos por PCI-DSS como lo indicado por el Consejo de Normas de Seguridad de PCI, que es un esfuerzo conjunto de marcas como Visa, MasterCard, American Express y Discover.<br><br>
            Los requisitos del PCI-DSS ayudan a garantizar el manejo seguro de la información de tarjetas de crédito de las tiendas y sus proveedores de servicios.<br><br>
            Para una visión más clara, es posible que también desees leer las Condiciones de servicio de Shopify aquí o Declaración de privacidad aquí.<br>
        </div>

        <div class="col-12" style="margin-top:2em; color:grey;">
            <b style="color:black">SECCIÓN 5 - SERVICIOS DE TERCERAS PARTES</b><br><br>
            Nuestra tienda se encuentra alojada en Shopify Inc. Ellos nos proporcionan la plataforma de comercio electrónico en línea que nos permite venderte nuestros productos y servicios.<br><br>

            En general, los proveedores de terceras partes utilizados por nosotros solo recopilarán, usarán y divulgarán tu información en la medida que sea necesaria para que les permita desempeñar los servicios que nos proveen.<br><br>

            Sin embargo, algunos proveedores de servicios de terceros, tales como pasarelas de pago y otros procesadores de transacciones de pago, tienen sus propias poliíticas de privacidad con respecto a la información que estamos obligados a proporcionarles para las transacciones relacionadas con las compras.<br><br>

            Para estos proveedores, te recomendamos que leas las poliíticas de privacidad para que puedas entender la manera en que tu información personal será manejada.<br><br>

            En particular, recuerda que algunos proveedores pueden estar ubicados o contar con instalaciones que se encuentran en una jurisdicción diferente a ti o nosotros. Asique si deseas proceder con una transacción que involucra los servicios de un proveedor a terceros, tu información puede estar sujeta a las leyes de la jurisdicción (jurisdicciones) en que se encuentra el proveedor de servicios o sus instalaciones.<br><br>

            A modo de ejemplo, si te encuentras en Canadá y tu transacción es procesada por una pasarela de pago con sede en Estados Unidos, entonces tu información personal utilizada para completar la transacción puede ser sujeto de divulgación en virtud de la legislación de Estados Unidos, incluyendo la Ley Patriota.<br><br>

            Una vez que abandonas el sitio web de nuestra tienda o te rediriges a un sitio o aplicación de terceros, ya no estás siendo regulados por la presente Política de Privacidad o los Términos de Servicio de nuestra página web.<br><br>

            <b style="color:black; font-size:0.9em;">Enlaces</b><br><br>

            Cuando haces clic en enlaces de nuestra tienda, puede que seas redirigido fuera de nuestro sitio. No somos reponsables por las prácticas de privacidad de otros sitios y te recomendamos leer sus normas de privacidad.<br>

        </div>

        <div class="col-12" style="margin-top:2em; color:grey;">
                <b style="color:black">SECCIÓN 6 - SEGURIDAD</b><br><br>
                Para proteger tu información personal, tomamos precauciones razonables y seguimos las mejores prácticas de la industria para asegurarnos de que no haya pérdida de manera inapropiada, mal uso, acceso, divulgación, alteración o destrucción de la misma.<br><br>

                SI nos proporcionas la información de tu tarjeta de crédito, dicha información es encriptada mediante la tecnología Secure Socket Layer (SSL) y se almacena con un cifrado AES-256. Aunque ningún método de transmisión a través de Internet o de almacenamiento electrónico es 100% seguro, seguimos todos los requisitos de PCI-DSS e implementamos normas adicionales aceptadas por la industria.<br><br>

                <b style="color:black; font-size:0.9em;">COOKIES</b><br><br>

                Aquí tienes una lista de cookies que utilizamos. Las enlistamos para que puedas elegir si quieres optar por quitarlas o no<br><br>

                <ul>
                    <li>_session_id: Token único, por sesión. Permite a Shopify almacenar información sobre tu sesión (referente, página de inicio, etc).</li>
                    <li>_shopify_visit: No almacena datos, persistente por 30 minutos desde la última visita. Usado por nuestro por el rastreador interno de estadísticas de nuestro proveedor de sitio web para registrar el número de visitas.</li>
                    <li>_shopify_uniq: No almacena datos, expira a medianoche (relativa al visitante) del siguiente día. Cuenta el número de visitas a una tienda por un sólo cliente.</li>
                    <li>cart: Token único, persistente por 2 semanas. Almacena información sobre el contenido de tu carrito.</li>
                    <li>_secure_session_id: Token único, por sesión.</li>
                    <li>storefront_digest: Token único, indefinida si la tienda tiene contraseña. Usado para determinar si el visitante actual tiene acceso.</li>
                </ul>
    
            </div>

            <div class="col-12" style="margin-top:2em; color:grey;">
                <b style="color:black">SECCIÓN 7 - EDAD DE CONSENTIMIENTO</b><br><br>
                Al utilizar este sitio, declaras que tienes al menos la mayoría de edad en tu estado o provincia de residencia, o que tienes la mayoría de edad en tu estado o provincia de residencia y que nos has dado tu consentimiento para permitir que cualquiera de tus dependientes menores use este sitio.<br><br>
            </div>

            <div class="col-12" style="margin-top:2em; color:grey;">
                <b style="color:black">SECCIÓN 8 - CAMBIOS A ESTA POLÍTICA DE PRIVACIDAD</b><br><br>
                Nos reservamos el derecho de modificar esta política de privacidad en cualquier momento, asique por favor revísala frecuentemente. Cambios y aclaraciones entrarán en vigencia inmediatamente después de su publicación en el sitio web. Si hacemos cambios materiales a esta política, notificaremos aquí que ha sido actualizada, por lo que cual estás enterado de qué información recopilamos, cómo y bajo qué circunstancias, si las hubiere, la utilizamos y/o divulgamos.<br><br>

                Si nuestra tienda es adquirida o fusionada con otra empresa, tu información puede ser transferida a los nuevos propietarios, para que podamos seguir vendiéndote productos.<br><br>
            </div>

            <div class="col-12" style="margin-top:2em; color:grey;">
                <b style="color:black">PREGUNTAS E INFORMACIÓN DE CONTACTO</b><br><br>
                Si quieres: acceder, corregir, enmendar o borrar cualquier información personal que poseamos sobre ti, registrar una queja, o simplemente quieres más información contacta a nuestro Oficial de Cumplimiento de Privacidad <span style="color:cornflowerblue;">vivian@engiegnv.com </span> o por correo postal a Engie GNV.<br><br>

                Prol. Miguel Alemán No. 4984<br>
                Col, Pedro Ignacio Mata, Enrique C. Rebsamen <br>
                C.P. 91960 <br>
                Veracruz, Veracruz.
            </div>

    </div>
</div>

@endsection