@extends('index')

@section('seccion')
    
    <div class="container contenedor_estaciones">
        <div class="row">
            <div class="col-lg-4 col-12">
                <p>
                    <h3 class="textos_estaciones">Estación Matriz</h3>
                    <h4 class="textos_estaciones">Dirección</h4>
                    <address class="textos_estaciones">
                        Prol. Miguel Alemán No. 4984<br>
                        Col, Pedro Ignacio Mata, Enrique C. Rebsamen <br>
                        C.P. 91960 <br>
                        Veracruz, Ver.
                    </address>
                </p>
            </div>
            <div class="col-lg-7 col-12 map">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3769.191938147076!2d-96.14787058509782!3d19.14307378704945!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85c34147b5d0600d%3A0x69065bb314181182!2sEngieGNV+MIGUEL+ALEMAN!5e0!3m2!1ses!2smx!4v1565210806799!5m2!1ses!2smx"></iframe>
            </div>
        </div>

        <hr>
        
        <div class="row">
            <div class="col-lg-4 col-12">
                <p>
                    <h3 class="textos_estaciones">Estación López Portillo</h3>
                    <h4 class="textos_estaciones">Dirección</h4>
                    <address class="textos_estaciones">
                        Avenida López Portillo<br>
                        C.P. 77536 <br>
                        Quintana Roo 
                    </address>
                </p>
            </div>
            <div class="col-lg-7 col-12 map">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3721.320407301515!2d-86.89652928506554!3d21.139643585938334!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zMjHCsDA4JzIyLjciTiA4NsKwNTMnMzkuNiJX!5e0!3m2!1sen!2smx!4v1565150909701!5m2!1sen!2smx"></iframe>
            </div>
        </div>

        <hr>

        <div class="row">
                <div class="col-lg-4 col-12">
                    <p>
                        <h3 class="textos_estaciones">Estación Niños Héroes</h3>
                        <h4 class="textos_estaciones">Dirección</h4>
                        <address class="textos_estaciones">
                            Av Niños Héroes<br>
                            C.P. 77516<br>
                            Quintana Roo <br>
                        </address>
                    </p>
                </div>
                <div class="col-lg-7 col-12 map">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3720.282930269443!2d-86.85074428506478!3d21.180916685916166!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zMjHCsDEwJzUxLjMiTiA4NsKwNTAnNTQuOCJX!5e0!3m2!1sen!2smx!4v1565150966973!5m2!1sen!2smx"></iframe>
                </div>
            </div>
    </div>

@endsection