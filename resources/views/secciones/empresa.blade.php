@extends('index')

@section('seccion')

<div class="container">
    <div class="row" style="margin-bottom:3em;">
        <div class="col-12">
            <div class="row">
                <div class="col-12 text-left" style="font-size:1.4em;">
                    <img style="width:100%; height:auto;" src="{{URL::asset('../img/empresa_engie.jpg')}}">
                </div>
            </div>

            <div class="row" style="margin-top:2em;">
                <div class="col-12">
                    <b style="font-size:1.7em;">Ahorra protegiendo el medio ambiente</b>

                    <p style="color:grey; font-size:0.9em; text-align:justify; margin-top:1em;">
                        <b class="flux">Engie GNV</b>: Es una empresa mexicana integrada por un equipo de profesionales emprendedores que buscan el desarrollo económico, ordenado, escalable y fundamentalmente sustentable del mercado de Gas Natural Vehicular (GNV) en México.<br><br>

                        <b class="flux">MISIÓN</b>: Vincular e invertir en el desarrollo e innovación de tecnologías, productos, procesos, servicios e investigaciones tecno-ambientalistas de calidad. Buscando siempre el beneficio directo al sector social en materia ecológica, tecnológica, económica y salubre con el desarrollo sustentable de la movilidad urbana.<br><br>
                        
                        <b class="flux">VISIÓN</b>: Ser una empresa de referencia nacional en el sector de combustibles sustentables y ecológicos.<br>
                    </p>
                </div>
            </div>

            <div class="row" style="margin-top:1em; margin-bottom:-1em;">
                <div class="col-12">
                    <b style="font-size:1.7em;">Valores</b>
                </div>
            </div>
            <div class="row" style="font-size:0.9em;">
                <div class="col-12 col-md-6 col-lg-3" style="margin-top:2em; color:grey;">
                    <b class="flux">SUSTENTABILIDAD</b><br>
                    Promover combustibles alternativos a la comunidad vehicular que ayuden a contribuir a la mejora del medio ambiente y la economía de los usuarios.
                </div>
                <div class="col-12 col-md-6 col-lg-3" style="margin-top:2em; color:grey;">
                    <b class="flux">INNOVACIÓN</b><br>
                    Incentivar el uso de tecnologías de última generación en el rubro de energéticos, así como la estimulación del desarrollo e investigación de los mismos.
                </div>
                <div class="col-12 col-md-6 col-lg-3" style="margin-top:2em; color:grey;">
                    <b class="flux">COLABORACIÓN</b><br>
                    Generar alianzas estratégicas con el fin de desarrollar en conjunto objetivos comunes e intereses afines, logrando así, potencializar las metas proyectadas, el desarrollo personal y profesional de nuestros colaboradores.
                </div>
                <div class="col-12 col-md-6 col-lg-3" style="margin-top:2em; color:grey;">
                    <b class="flux">INTEGRIDAD</b><br>
                    Propiciar un ambiente de trabajo en Engie GNV ecuánime que englobe de manera transparente y eficiente cada uno de nuestros valores empresariales.
                </div>
            </div>
        </div>
    </div>
</div>

@endsection