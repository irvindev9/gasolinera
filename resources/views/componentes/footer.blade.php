<footer class="row fix-row footer">
    <div class="col-12 fix-col foot">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-6 col-12 d-flex align-items-center justify-content-center">
                    
                    <p style="text-align:right;color:white;width:173px;font-size:15px"><img style="height:auto; width:100%;" src="/img/EngieAzul.png"><br><br>GNV</p>
                </div>
                <div class="col-lg-3 col-md-6 col-12">
                    <small class="white mini">
                        <b>Dirección Estación Matriz </b>
                        <br>
                        Prol. Miguel Alemán No. 4984<br>
                        Col, Pedro Ignacio Mata, Enrique C. Rebsamen <br>
                        C.P. 91960 <br>
                        Veracruz, Veracruz.
                        <br>
                        Tel: (555) 102-6036 {{--| Cel: 045 (443) 231-5345 --}}
                        <br>
                        <a style="text-decoration:none;color:white;" href="vivian.echemendia@engie.com">vivian.echemendia@engie.com</a>
                    </small>
                </div>
                <div class="col-lg-3 col-md-6 col-12"></div>
                <div class="col-lg-3 col-md-6 col-12">
                    <p class="white">
                        Visita nuestro Blog para conocer más sobre el Gas GNV
                        <hr style="background:white;">
                        <a href="/politica_privacidad" style="color:white;font-size:13px;">Conoce nuestra Política de Privacidad</a>
                    </p>
                </div>
            </div>
        </div>
    </div>
</footer>