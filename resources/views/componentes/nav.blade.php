<div class="row fix-row">
    <div class="col-12 fix-col d-none d-md-block">
        <ul class="nav justify-content-center nav_header">
            <li class="nav-item ind_item redondear {{$active[0]}}" style="background-color:#00AEEF!important;">
                <a class="nav-link indicadores" href="/">Inicio &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;</a>
            </li>
            <li class="nav-item ind_item redondear {{$active[1]}}" style="background-color:#FED11F!important;margin-left:-50px;">
                <a class="nav-link indicadores" href="/gasnatural">Gas Natural &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;</a>
            </li>
            <li class="nav-item ind_item redondear {{$active[2]}}" style="background-color:#1591CB!important;margin-left:-50px;">
                <a class="nav-link indicadores" href="/conversiones">Conversiones &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a> 
            </li>
            <li class="nav-item ind_item redondear {{$active[3]}}" style="background-color:#F3C031!important;margin-left:-50px;">
                <a class="nav-link indicadores" href="/estaciones">Estaciones&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a>
            </li>
            <li class="nav-item ind_item redondear {{$active[4]}}"  style="background-color:#275872!important;margin-left:-50px;">
                <a class="nav-link indicadores" href="/empresa">Empresa &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a>
            </li>
            <li class="nav-item ind_item redondear {{$active[5]}}"  style="background-color:#FED11F!important;margin-left:-50px;">
                <a class="nav-link indicadores" href="/facturacion">Facturación &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a>
            </li>
            <li class="nav-item ind_item redondear {{$active[6]}}"  style="background-color:#26a3db!important;margin-left:-50px;">
                <a class="nav-link indicadores" href="/contacto">Contacto &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a>
            </li>
            <li class="nav-item ind_item redondear {{$active[7]}}"  style="background-color:#F3C031!important;margin-left:-50px;">
                <a class="nav-link indicadores" href="/registro">Registro</a>
            </li>
        </ul>
    </div>
    <div class="col-12 fix-col d-block d-md-none">
        <!-- Responsive navbar -->
        <nav class="navbar navbar-expand-lg  navbar-dark bg-dark bg-light navbarNav">
                {{-- <a class="navbar-brand" href="#">GASECO</a> --}}
                <button class="navbar-toggler burger-btn" type="button" data-toggle="collapse" data-target="#navbarNav"
                    aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon white"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarNav">
                    <ul class="navbar-nav center">
                            <li class="nav-item ind_item {{$active[0]}}">
                                    <a class="nav-link indicadores2" href="/">Inicio</a>
                                </li>
                                <li class="nav-item ind_item {{$active[1]}}">
                                    <a class="nav-link indicadores2" href="/gasnatural">Gas Natural</a>
                                </li>
                                <li class="nav-item ind_item {{$active[2]}}">
                                    <a class="nav-link indicadores2" href="/conversiones">Conversiones</a>
                                </li>
                                <li class="nav-item ind_item {{$active[3]}}">
                                    <a class="nav-link indicadores2" href="/estaciones">Estaciones</a>
                                </li>
                                <li class="nav-item ind_item {{$active[4]}}">
                                    <a class="nav-link indicadores2" href="/empresa">Empresa</a>
                                </li>
                                <li class="nav-item ind_item {{$active[5]}}">
                                    <a class="nav-link indicadores2" href="/facturacion">Facturación</a>
                                </li>
                                <li class="nav-item ind_item {{$active[6]}}">
                                    <a class="nav-link indicadores2" href="/contacto">Contacto</a>
                                </li>
                                <li class="nav-item ind_item {{$active[7]}}">
                                    <a class="nav-link indicadores2" href="/registro">Registro</a>
                                </li>
                    </ul>
                </div>
            </nav>
    </div>
</div>