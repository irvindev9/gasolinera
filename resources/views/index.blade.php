<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="shortcut icon"  href="/img/favicon.ico" />
    <title>ENGIE - Gas Natural Vehicular</title>

    <!-- Jquery JS -->
    <script type="text/javascript" src="{{ asset('js/jquery-3.3.1.js') }}"></script>
    <!-- Jquery Mask JS -->
    <script type="text/javascript" src="{{ asset('js/jquery.mask.js') }}"></script>
    <!-- Bootstrap css -->
    <link href="{{ asset('bootstrap/css/bootstrap.css') }}" rel="stylesheet">
    <!-- Bootstrao JS -->
    <script type="text/javascript" src="{{ asset('bootstrap/js/bootstrap.js') }}"></script>
    <!-- Numeral JS -->
    <script type="text/javascript" src="{{ asset('js/numeral.js') }}"></script>


    {{-- <link rel="stylesheet" href="/css/app.css"> --}}
    <link rel="stylesheet" href="/css/estilos.css">
    {{-- <script src="/js/app.js"></script> --}}
</head>
<body>
    @include('componentes.logo')

    @include('componentes.nav')

    <div>
        @yield('seccion')
    </div>
    

    @include('componentes.footer')
</body>
</html>