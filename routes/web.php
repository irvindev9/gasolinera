<?php

Route::get('/', function () {
    for($i = 0;$i < 8;$i++){
        $active[$i] = '';
    }
    $active[0] = 'active';
    return view('secciones.inicio',['active' => $active]);
});

Route::get('/politica_privacidad', function () {
    for($i = 0;$i < 8;$i++){
        $active[$i] = '';
    }
    $active[0] = 'active';
    return view('secciones.politica_privacidad',['active' => $active]);
});


Route::any('/{seccion}',function($seccion){
    for($i = 0;$i < 8;$i++){
        $active[$i] = '';
    }

    switch($seccion){
        case 'inicio':
            $active[0] = 'active';
            return view('secciones.inicio',['active' => $active]);
        break;

        case 'gasnatural':
            $active[1] = 'active';
            return view('secciones.gasnatural',['active' => $active]);
        break;

        case 'conversiones':
            $active[2] = 'active';
            return view('secciones.conversiones',['active' => $active]);
        break;

        case 'estaciones':
            $active[3] = 'active';
            return view('secciones.estaciones',['active' => $active]);
        break;

        case 'empresa':
            $active[4] = 'active';
            return view('secciones.empresa',['active' => $active]);
        break;

        case 'facturacion':
            $active[5] = 'active';
            return view('secciones.facturacion',['active' => $active]);
        break;

        case 'contacto':
            $active[6] = 'active';
            return view('secciones.contacto',['active' => $active]);
        break;

        case 'registro':
            $active[7] = 'active';
            return view('secciones.registro',['active' => $active]);
        break;

        case 'politica_privacidad':
            $active[7] = 'active';
            return view('secciones.registro',['active' => $active]);
        break;
    }
});

//Save data
Route::any('/contacto/submit','ContactoController@store');

//Save data
Route::any('/registro/submit','RegistroController@store');

Route::post('/help/message','HelpController@help')->name('help_message');


