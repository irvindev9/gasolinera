<?php

namespace App\Http\Controllers;

use App\registro;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\NewRegister;

class RegistroController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //save info registro
        $row = new registro();
        $row->name = $request->name;
        $row->phone = $request->phone;
        $row->email = $request->email;
        $row->address = $request->address;
        $row->city = $request->city;
        $row->save();

        $mailData = $request;
        Mail::to('gustavo.maldo44@gmail.com')->send( new NewRegister( $mailData ) );


        return redirect('/registro?ok');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\registro  $registro
     * @return \Illuminate\Http\Response
     */
    public function show(registro $registro)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\registro  $registro
     * @return \Illuminate\Http\Response
     */
    public function edit(registro $registro)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\registro  $registro
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, registro $registro)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\registro  $registro
     * @return \Illuminate\Http\Response
     */
    public function destroy(registro $registro)
    {
        //
    }
}
