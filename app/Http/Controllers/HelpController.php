<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\help_users;
class HelpController extends Controller
{
    public function help( Request $request){


        $messages=[ 
            'name.required' => 'El campo nombre es obligatorio',
            'email.required' => 'El campo correo electronico es obligatorio',
            'message.required'=> 'El campo de mensaje es obligatorio'
        ];
        $rules = [
            'name' => 'required',
            'email' => 'required',
            'message' => 'required|max:500'
        ];
        
        $this->validate($request,$rules, $messages);

        $help = new Help_users();
        $help->name= $request->name;
        $help->email= $request->email;
        $help->message= $request->message;
        $help->save();
        return back();
    }
}
